use convert_case::{Case, Casing};
use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::quote;
use syn::{parse_macro_input, Attribute, Ident, Token};

#[proc_macro_attribute]
pub fn number_enum(args: TokenStream, input: TokenStream) -> TokenStream {
  let input: syn::ItemEnum = parse_macro_input!(input);
  let mut fields = vec![];
  let mut values = vec![];
  for v in &input.variants {
    fields.push(&v.ident);
    values.push(&v.discriminant.as_ref().expect("all variants must contain a discriminant").1);
  }
  let name = &input.ident;
  quote! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, serde_repr::Serialize_repr, serde_repr::Deserialize_repr)]
    #[repr(u32)]
    #input

    impl From<u32> for #name {
      fn from(v: u32) -> Self {
        match v {
          #(
            #values => Self::#fields,
          )*
          v => panic!("invalid number {}", v)
        }
      }
    }

    impl From<#name> for u32 {
      fn from(s: #name) -> u32 {
        match s {
          #(
            #name::#fields => #values,
          )*
        }
      }
    }
  }.into()
}

#[proc_macro_attribute]
pub fn string_enum(args: TokenStream, input: TokenStream) -> TokenStream {
  let mut input: syn::ItemEnum = parse_macro_input!(input);
  let mut fields = vec![];
  let mut values = vec![];
  for v in &mut input.variants {
    values.push(v.ident.to_string());
    let doc = format!("This variant matches the string `\"{}\"`", v.ident);
    v.ident = Ident::new(&v.ident.to_string().to_case(Case::Pascal), v.ident.span());
    fields.push(v.ident.clone());
    if v.discriminant.is_some() {
      panic!("no variant can have a discriminant");
    }
    v.attrs.push(syn::Attribute {
      pound_token:   Token![#](Span::call_site()),
      style:         syn::AttrStyle::Outer,
      bracket_token: syn::token::Bracket { span: Span::call_site() },
      path:          Ident::new("doc", Span::call_site()).into(),
      tokens:        quote!(= ""),
    });
    v.attrs.push(syn::Attribute {
      pound_token:   Token![#](Span::call_site()),
      style:         syn::AttrStyle::Outer,
      bracket_token: syn::token::Bracket { span: Span::call_site() },
      path:          Ident::new("doc", Span::call_site()).into(),
      tokens:        quote!(= #doc),
    });
  }
  let name = &input.ident;
  let name_visitor = Ident::new(&(input.ident.to_string() + "Visitor"), input.ident.span());
  quote! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
    /// This is a string enum. This type can be converted to/from a `&str`
    /// with the `From` trait. If this enum is invalid, then we want to
    /// close the connection with discord anyway, so I opted to panic
    /// (for now).
    ///
    /// The enum value is in `PascalCase`, which is not the string we are looking for. See the docs for each item for the string it matches.
    #input

    impl From<&str> for #name {
      fn from(v: &str) -> Self {
        match v {
          #(
            #values => Self::#fields,
          )*
          v => panic!("invalid variant {}", v)
        }
      }
    }

    impl From<&#name> for &'static str {
      fn from(s: &#name) -> &'static str {
        match s {
          #(
            #name::#fields => #values,
          )*
        }
      }
    }

    impl serde::Serialize for #name {
      fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
      where
          S: serde::Serializer,
      {
        serializer.serialize_str(self.into())
      }
    }

    struct #name_visitor;
    impl<'de> serde::de::Visitor<'de> for #name_visitor {
      type Value = #name;

      fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str(concat!("a ", stringify!(#name), " (a string that will be parsed as a variant)"))
      }

      fn visit_str<E>(self, v: &str) -> Result<#name, E>
      where
          E: serde::de::Error,
      {
        Ok(v.into())
      }
    }

    impl<'de> serde::Deserialize<'de> for #name {
      fn deserialize<D>(deserializer: D) -> Result<#name, D::Error>
      where
          D: serde::Deserializer<'de>,
      {
        deserializer.deserialize_str(#name_visitor)
      }
    }
  }
  .into()
}

struct Bitfield {
  name:   Ident,
  fields: Vec<Field>,
}

struct Field {
  attrs: Vec<Attribute>,
  name:  Ident,
  lit:   syn::LitInt,
}

impl syn::parse::Parse for Bitfield {
  fn parse(input: syn::parse::ParseStream<'_>) -> syn::Result<Self> {
    let name: Ident = input.parse()?;
    input.parse::<syn::token::Comma>()?;
    let mut fields = vec![];

    loop {
      let attrs: Vec<Attribute> = input.call(Attribute::parse_outer)?;
      let name: Ident = input.parse()?;
      input.parse::<syn::token::Eq>()?;
      let lit: syn::LitInt = input.parse()?;
      input.parse::<syn::token::Comma>()?;
      if input.is_empty() {
        break;
      }
      fields.push(Field { attrs, name, lit });
    }

    Ok(Bitfield { name, fields })
  }
}

#[proc_macro]
pub fn bitfield(input: TokenStream) -> TokenStream {
  let input: Bitfield = parse_macro_input!(input);
  let mut attrs = vec![];
  let mut names = vec![];
  let mut title_names = vec![];
  let mut is_name = vec![];
  let mut set_name = vec![];
  let mut with_name = vec![];
  let mut values = vec![];
  let mut values_str = vec![];
  let mut has_attrs = false;
  for v in &input.fields {
    if !v.attrs.is_empty() {
      has_attrs = true;
    }
    attrs.push(&v.attrs);
    names.push(v.name.to_string());
    title_names.push(v.name.to_string().to_case(Case::Title));
    is_name.push(Ident::new(&format!("is_{}", v.name), v.name.span()));
    set_name.push(Ident::new(&format!("set_{}", v.name), v.name.span()));
    with_name.push(Ident::new(&format!("with_{}", v.name), v.name.span()));
    values.push(&v.lit);
    values_str.push(v.lit.to_string());
  }
  let name = &input.name;
  let item_docs = if has_attrs {
    quote! {
      /// # Docs for each bit
      #(
        #[doc = concat!(
          "## ",
          #title_names,
        )]
        #[doc = ""]
        #[doc = concat!(
          "Bit ",
          #values,
          ", named `",
          #names,
          "`",
        )]
        #[doc = ""]
        #(
          #attrs
        )*
      )*
    }
  } else {
    quote! {}
  };
  let out = quote! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
    #[serde(transparent)]
    /// Bitfields have a number of bits that can be set or unset.
    /// These can be easily accessed with the `is_`, `set_`, and
    /// `with_` functions. This bitfield has the following layout:
    ///
    ///  Bit | Offset | Accessible with
    /// -----|--------|-----------------
    #(
      #[doc = concat!(
        #names, " | ",
        #values, " | ",
        concat!("`self.0 & (1 << ", #values, ") != 0` or [`is_", #names, "`](", stringify!(#name), "::is_", #names, ")"),
      )]
    )*
    ///
    /// If a bitfield is recieved directly from discord, it is
    /// considered invalid if any of the set bits are not listed
    /// in the above table. Currently, this is not checked.
    ///
    /// If you are sending a bitfield to discord, any bits not
    /// listed in the above table will be ignored. This should be
    /// avoided, and probably throw an error as well, but it is
    /// unchecked on both sides of the connection.
    ///
    #item_docs
    pub struct #name(pub u32);

    impl #name {
      #(
        /// Checks if bit
        #[doc = #values_str]
        /// is set.
        pub const fn #is_name(&self) -> bool {
          self.0 & (1 << #values) != 0
        }
      )*
      #(
        /// Sets/clears bit
        #[doc = concat!(#values_str, ".")]
        pub fn #set_name(&mut self, present: bool) {
          if present {
            self.0 |= (1 << #values)
          } else {
            self.0 &= !(1 << #values)
          }
        }
      )*
      #(
        /// Sets bit
        #[doc = concat!(#values_str, ",")]
        /// and returns the modified value. This can be used to create a bitfield in one line.
        pub const fn #with_name(mut self) -> Self {
          self.0 |= (1 << #values);
          self
        }
      )*
    }
  };

  out.into()
}
