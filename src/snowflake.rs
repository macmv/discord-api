use serde::{de, de::Visitor, Deserialize, Deserializer, Serialize, Serializer};
use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Snowflake(pub u64);

impl fmt::Display for Snowflake {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", self.0)
  }
}

impl Serialize for Snowflake {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    serializer.serialize_str(&self.0.to_string())
  }
}

struct SnowflakeVisitor;
impl<'de> Visitor<'de> for SnowflakeVisitor {
  type Value = Snowflake;

  fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
    formatter.write_str("a snowflake (a string that will be parsed as an int)")
  }

  fn visit_str<E>(self, v: &str) -> Result<Snowflake, E>
  where
    E: de::Error,
  {
    Ok(Snowflake(v.parse().map_err(|e| E::custom(format!("invalid snowflake: {}", e)))?))
  }
}

impl<'de> Deserialize<'de> for Snowflake {
  fn deserialize<D>(deserializer: D) -> Result<Snowflake, D::Error>
  where
    D: Deserializer<'de>,
  {
    deserializer.deserialize_str(SnowflakeVisitor)
  }
}

impl PartialEq<u64> for Snowflake {
  fn eq(&self, other: &u64) -> bool {
    self.0 == *other
  }
}
impl PartialEq<Snowflake> for u64 {
  fn eq(&self, other: &Snowflake) -> bool {
    *self == other.0
  }
}
