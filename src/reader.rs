use crate::{
  types::{Guild, Interaction, MemberUpdate, Message, ReadyInfo},
  Discord, GatewayReceiver,
};
use std::sync::Arc;

pub struct DiscordReader {
  rx:      GatewayReceiver,
  discord: Arc<Discord>,
}

impl DiscordReader {
  pub fn new(rx: GatewayReceiver, discord: Arc<Discord>) -> Self {
    DiscordReader { rx, discord }
  }
}

/// A Discord API event. Any time anything happens on discord, an event will be
/// sent to you. This enum is how you can handle those events.
///
/// This is marked as `#[non_exhaustive]` because of Discord API updates. I
/// don't want a new API version to break user-space code.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum Event {
  // ################ Bit 0 ################
  /// Sent when a guild is created, or when the bot logs in (after a `Ready`
  /// event).
  GuildCreate(Guild),

  // ################ Bit 1 ################
  /// Sent whenever a member of a guild is updated. This includes role changes,
  /// permission changes, and being removed from a guild.
  MemberUpdate(MemberUpdate),

  // ################ Bit 9 ################
  /// Sent when a message is created in a text channel.
  MessageCreate(Message),
  /// Sent when a message is edited/changed. This could be a reaction being
  /// added or the content being changed.
  MessageUpdate(Message),
  /// Sent when a message is deleted in a text channel.
  MessageDelete(Message),

  /// Sent once a connection has been established.
  Ready,
  /// Sent when a slash command has been executed by a client.
  InteractionCreate,
}

macro_rules! handle_event {
  ($event:expr, $variant:ident, $content:block,) => {{
    $content
    Event::$variant
  }};
  ($event:expr, $variant:ident, $ty:ty,) => {{
    let mut info: $ty = $event.parse();
    Event::$variant(info)
  }};
  ($event:expr, $variant:ident, $ty:ty, $info:ident,) => {{
    let mut $info: $ty = $event.parse();
    Event::$variant($info)
  }};
  ($event:expr, $variant:ident, $ty:ty, $info:ident, $content:block,) => {{
    let mut $info: $ty = $event.parse();
    $content
    Event::$variant($info)
  }};
}

macro_rules! event {
  ($event:expr => {
    $( $name:expr => Event::$variant:ident$(($ty:ty))?$(: $arg:ident)? $(=> $content:block)?, )*
  }) => {
    #[allow(unused_mut)]
    match $event.t.as_ref().unwrap().as_str() {
      $(
      $name => handle_event!($event, $variant, $($ty,)? $($arg,)? $($content,)?),
      )*
      t => unimplemented!("unknown event: {}", t),
    }
  }
}

impl Iterator for DiscordReader {
  type Item = Event;
  fn next(&mut self) -> Option<Event> {
    let event = self.rx.recv()?;
    Some(event!(event => {
      // Bit 0 (guilds)
      "GUILD_CREATE" => Event::GuildCreate(Guild): guild => {
        guild.populate(&self.discord);
        self.discord.guilds.lock().push_or_replace(guild.id, guild.clone());
      },
      // Bit 1 (guild_members)
      "GUILD_MEMBER_UPDATE" => Event::MemberUpdate(MemberUpdate),
      // Bit 9 (guild_messages)
      "MESSAGE_CREATE" => Event::MessageCreate(Message): msg => {
        msg.populate(&self.discord);
      },
      "MESSAGE_UPDATE" => Event::MessageUpdate(Message),
      "MESSAGE_DELETE" => Event::MessageDelete(Message),
      // Other
      "READY" => Event::Ready => {
        let info: ReadyInfo = event.parse();
        let mut lock = self.discord.ready.write();
        if lock.is_some() {
          panic!("got READY event twice");
        }
        *lock = Some(info);
      },
      "INTERACTION_CREATE" => Event::InteractionCreate => {
        let int: Interaction = event.parse();
        let data = int.data.unwrap();
        self
          .discord
          .commands
          .lock()
          .get(&data.id.unwrap())
          .expect("discord sent invalid command id")
          .run(int.member.unwrap().user().id, &data);
      },
      // t => unimplemented!("unknown event: {}", t),
    }))
  }
}
