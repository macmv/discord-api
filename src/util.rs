use serde::{
  de::{SeqAccess, Visitor},
  ser::SerializeSeq,
  Deserialize, Deserializer, Serialize, Serializer,
};
use std::{borrow::Borrow, collections::HashMap, fmt, hash::Hash, marker::PhantomData, mem};

/// A helper trait for an ordered map. This is used when deserializing structs,
/// so that the ordered map knows how to produce a key for each value. This is
/// needed because an ordered map is deserialized just a like a `Vec<V>`, and we
/// don't have any keys avalible at the time of creating that `Vec<V>`.
pub trait OrderedMapKey<K> {
  fn key(&self) -> K;
}

pub struct OrderedMap<K, V> {
  items: Vec<V>,
  map:   HashMap<K, usize>,
}

impl<K, V> Default for OrderedMap<K, V> {
  fn default() -> Self {
    OrderedMap::new()
  }
}

impl<K, V> OrderedMap<K, V> {
  pub fn new() -> Self {
    OrderedMap { items: vec![], map: HashMap::new() }
  }

  pub fn with_capacity(cap: usize) -> Self {
    OrderedMap { items: Vec::with_capacity(cap), map: HashMap::with_capacity(cap) }
  }
}

impl<K: Hash + Eq, V> OrderedMap<K, V> {
  pub fn from_vec_with_fn<F: Fn(&V) -> K>(vec: Vec<V>, key: F) -> Self {
    let mut out = OrderedMap::with_capacity(vec.len());
    for val in vec {
      let key = key(&val);
      if out.map.insert(key, out.items.len()).is_some() {
        panic!("could not convert vec to OrderedMap, the same key was returned multiple times");
      }
      out.items.push(val);
    }
    out
  }
  pub fn from_vec(vec: Vec<V>) -> Self
  where
    V: OrderedMapKey<K>,
  {
    let mut out = OrderedMap::with_capacity(vec.len());
    for val in vec {
      let key = val.key();
      if out.map.insert(key, out.items.len()).is_some() {
        // panic!("could not convert vec to OrderedMap, the same key was
        // returned multiple times");
      }
      out.items.push(val);
    }
    out
  }
  /// Gets a specific item by it's key.
  pub fn get<Q: ?Sized>(&self, key: &Q) -> Option<&V>
  where
    K: Borrow<Q>,
    Q: Hash + Eq,
  {
    Some(&self.items[*self.map.get(key)?])
  }
  /// Gets a specific item by it's index.
  pub fn get_idx(&self, idx: usize) -> Option<&V> {
    self.items.get(idx)
  }
  /// Returns the number of items in this map.
  pub fn len(&self) -> usize {
    self.items.len()
  }
  /// Returns true if this map is empty.
  pub fn is_empty(&self) -> bool {
    self.items.is_empty()
  }
  /// Returns an iterator over all the values in this map. This will be in
  /// order.
  pub fn iter_mut(&mut self) -> std::slice::IterMut<V> {
    self.items.iter_mut()
  }
  /// This is the simplest operation on an OrderedMap. If the key doesn't exist,
  /// then this pushes the new value to the end of the list. If the key already
  /// exists, then this replaces the old value with the new one. If the key was
  /// already present, this returns the old value.
  pub fn push_or_replace(&mut self, key: K, mut val: V) -> Option<V> {
    if let Some(idx) = self.map.get(&key) {
      mem::swap(&mut self.items[*idx], &mut val);
      Some(val)
    } else {
      self.map.insert(key, self.items.len());
      self.items.push(val);
      None
    }
  }
}

impl<K, V> fmt::Debug for OrderedMap<K, V>
where
  V: fmt::Debug,
{
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "OrderedMap")?;
    let mut list = f.debug_list();
    for v in &self.items {
      list.entry(&v);
    }
    list.finish()
  }
}

impl<K, V> Clone for OrderedMap<K, V>
where
  K: Clone,
  V: Clone,
{
  fn clone(&self) -> Self {
    OrderedMap { items: self.items.clone(), map: self.map.clone() }
  }
}

impl<K, V> Serialize for OrderedMap<K, V>
where
  V: Serialize,
{
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    let mut seq = serializer.serialize_seq(Some(self.items.len()))?;
    for v in &self.items {
      seq.serialize_element(v)?;
    }
    seq.end()
  }
}

struct OrderedMapVisitor<K, V> {
  marker: PhantomData<fn() -> OrderedMap<K, V>>,
}
impl<'de, K, V> Visitor<'de> for OrderedMapVisitor<K, V>
where
  V: Deserialize<'de> + OrderedMapKey<K>,
  K: Hash + Eq,
{
  type Value = OrderedMap<K, V>;

  fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
    formatter.write_str("a snowflake (a string that will be parsed as an int)")
  }

  fn visit_seq<S>(self, mut access: S) -> Result<OrderedMap<K, V>, S::Error>
  where
    S: SeqAccess<'de>,
  {
    let mut out = Vec::with_capacity(access.size_hint().unwrap_or(0));
    while let Some(item) = access.next_element()? {
      out.push(item);
    }
    Ok(OrderedMap::from_vec(out))
  }
}

impl<'de, K, V> Deserialize<'de> for OrderedMap<K, V>
where
  V: Deserialize<'de> + OrderedMapKey<K>,
  K: Hash + Eq,
{
  fn deserialize<D>(deserializer: D) -> Result<OrderedMap<K, V>, D::Error>
  where
    D: Deserializer<'de>,
  {
    deserializer.deserialize_seq(OrderedMapVisitor { marker: PhantomData::default() })
  }
}
