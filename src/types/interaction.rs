use super::{
  AppCommandOptionType, AppCommandOptionValue, Channel, Emoji, Member, Message, Role, User,
};
use crate::{
  util::{OrderedMap, OrderedMapKey},
  Snowflake,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Deserialize)]
pub struct Interaction {
  /// ID of the interaction.
  pub id:             Snowflake,
  /// ID of the application this interaction is for.
  pub application_id: Snowflake,
  /// The type of interaction.
  #[serde(rename = "type")]
  pub ty:             InteractionType,
  /// The command data payload.
  pub data:           Option<InteractionData>,
  /// The guild it was sent from. May be None if this was in a DM.
  pub guild_id:       Option<Snowflake>,
  /// The channel it was sent from.
  pub channel_id:     Snowflake,
  /// Guild member data for the invoking user, including permissions. This
  /// will always be sent when interacting in a guild.
  pub member:         Option<Member>,
  /// User object for the invoking user, if invoked in a DM. This will always be
  /// sent when interacting in a DM.
  pub user:           Option<User>,
  /// A continuation token for responding to the interaction.
  pub token:          String,
  /// Read-only property, always 1.
  pub version:        u32,
  /// For components, the message they were attached to.
  pub message:        Option<Message>,
}

#[discord_macros::number_enum]
pub enum InteractionType {
  Ping               = 1,
  ApplicationCommand = 2,
  MessageComponent   = 3,
}

#[derive(Debug, Clone, Deserialize)]
pub struct InteractionData {
  /// The ID of the invoked command. Only present if the interaction was an
  /// application command.
  pub id:             Option<Snowflake>,
  /// The name of the invoked command. Only present if the interaction was an
  /// application command.
  pub name:           Option<String>,
  /// The type of the invoked command. Only present if the interaction was an
  /// application command.
  #[serde(rename = "type")]
  pub ty:             Option<u32>,
  /// Converted users + roles + channels. Only present if the interaction was
  /// an application command.
  pub resolved:       Option<ResolvedData>,
  /// The params + values from the user. Only present if the interaction was
  /// an application command.
  #[serde(default = "Default::default")]
  pub options:        OrderedMap<String, InteractionOption>,
  /// The `custom_id` of the component. Only present if the interaction was a
  /// component.
  pub custom_id:      Option<String>,
  /// The type of the component. Only present if the interaction was a
  /// component.
  pub component_type: Option<u32>,
  /// The values the user selected. Only present if the interaction was a
  /// component.
  #[serde(default = "Vec::default")]
  pub values:         Vec<SelectedOption>,
  /// ID the of user or message targetted by a user or message command. Only
  /// present if the interaction was a user or message command.
  pub target_id:      Option<Snowflake>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ResolvedData {
  /// Users referenced in the interaction.
  #[serde(default = "HashMap::default")]
  pub users:    HashMap<Snowflake, User>,
  /// Members referenced in the interaction. They will be missing their `user`
  /// fields, but the `users` map in this struct will contain the data for these
  /// users.
  #[serde(default = "HashMap::default")]
  pub members:  HashMap<Snowflake, Member>,
  /// Roles referenced in the interaction.
  #[serde(default = "HashMap::default")]
  pub roles:    HashMap<Snowflake, Role>,
  /// Channels referenced in the interaction.
  #[serde(default = "HashMap::default")]
  pub channels: HashMap<Snowflake, Channel>,
  /// Messages referenced in the interaction.
  #[serde(default = "HashMap::default")]
  pub messages: HashMap<Snowflake, Message>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InteractionOption {
  /// The name of the parameter.
  name:    String,
  /// The type of option.
  #[serde(rename = "type")]
  ty:      AppCommandOptionType,
  /// The value of this option. Will be a string, float, or integer.
  value:   serde_json::Value,
  /// Present for groups or sub commands.
  options: Option<Vec<InteractionOption>>,
  /// True if this is focused for auto complete.
  #[serde(default = "bool::default")]
  focused: bool,
}

impl OrderedMapKey<String> for InteractionOption {
  fn key(&self) -> String {
    self.name.clone()
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SelectedOption {
  /// The user-facing name of the option, max 100 characters.
  label:       String,
  /// The dev-define value of the option, max 100 characters.
  value:       String,
  /// An additional description of the option, max 100 characters.
  description: Option<String>,
  /// Contains ID, name, and animated.
  emoji:       Option<Emoji>,
  /// Will render this option as selected by default.
  #[serde(default = "bool::default")]
  default:     bool,
}

impl InteractionData {
  #[track_caller]
  pub fn option<T: From<AppCommandOptionValue>>(&self, name: &str) -> T {
    self
      .options
      .get(name)
      .unwrap_or_else(|| panic!("no option named `{name}` was given"))
      .clone()
      .into_value()
      .into()
  }
}

impl From<AppCommandOptionValue> for Snowflake {
  #[track_caller]
  fn from(v: AppCommandOptionValue) -> Self {
    match v {
      AppCommandOptionValue::User(id) => id,
      AppCommandOptionValue::Channel(id) => id,
      AppCommandOptionValue::Mentionable(id) => id,
      _ => panic!("cannot be converted into a snowflake: {:?}", v),
    }
  }
}
impl From<AppCommandOptionValue> for String {
  #[track_caller]
  fn from(v: AppCommandOptionValue) -> Self {
    match v {
      AppCommandOptionValue::String(v) => v,
      AppCommandOptionValue::Role(v) => v,
      _ => panic!("cannot be converted into a double: {:?}", v),
    }
  }
}
impl From<AppCommandOptionValue> for f64 {
  #[track_caller]
  fn from(v: AppCommandOptionValue) -> Self {
    match v {
      AppCommandOptionValue::Double(v) => v,
      _ => panic!("cannot be converted into a double: {:?}", v),
    }
  }
}
impl From<AppCommandOptionValue> for i64 {
  #[track_caller]
  fn from(v: AppCommandOptionValue) -> Self {
    match v {
      AppCommandOptionValue::Int(v) => v,
      _ => panic!("cannot be converted into an int: {:?}", v),
    }
  }
}
impl From<AppCommandOptionValue> for bool {
  #[track_caller]
  fn from(v: AppCommandOptionValue) -> Self {
    match v {
      AppCommandOptionValue::Bool(v) => v,
      _ => panic!("cannot be converted into a bool: {:?}", v),
    }
  }
}

impl InteractionOption {
  pub fn into_value(self) -> AppCommandOptionValue {
    match self.ty {
      AppCommandOptionType::SubCommand => todo!(),
      AppCommandOptionType::SubCommandGroup => todo!(),
      AppCommandOptionType::String => {
        AppCommandOptionValue::String(serde_json::from_value(self.value).unwrap())
      }
      AppCommandOptionType::Int => {
        AppCommandOptionValue::Int(serde_json::from_value(self.value).unwrap())
      }
      AppCommandOptionType::Bool => {
        AppCommandOptionValue::Bool(serde_json::from_value(self.value).unwrap())
      }
      AppCommandOptionType::User => {
        AppCommandOptionValue::User(serde_json::from_value(self.value).unwrap())
      }
      AppCommandOptionType::Channel => {
        AppCommandOptionValue::Channel(serde_json::from_value(self.value).unwrap())
      }
      AppCommandOptionType::Role => {
        AppCommandOptionValue::Role(serde_json::from_value(self.value).unwrap())
      }
      AppCommandOptionType::Mentionable => {
        AppCommandOptionValue::Mentionable(serde_json::from_value(self.value).unwrap())
      }
      AppCommandOptionType::Double => {
        AppCommandOptionValue::Double(serde_json::from_value(self.value).unwrap())
      }
    }
  }
}
