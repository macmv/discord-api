use super::ChannelType;
use crate::Snowflake;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AppCommand {
  /// Unique ID of the command.
  #[serde(skip_serializing_if = "Option::is_none")]
  pub id:                 Option<Snowflake>,
  /// The type of command. The default is `ChaitInput`.
  #[serde(rename = "type")]
  pub ty:                 AppCommandType,
  /// Unique ID of the parent application.
  pub application_id:     Snowflake,
  /// Guild ID of the command. Global command if not present.
  #[serde(skip_serializing_if = "Option::is_none")]
  pub guild_id:           Option<Snowflake>,
  /// 1-32 character name.
  pub name:               String,
  /// 1-100 character description for `ChatInput` commands, empty string for
  /// `User` and `Message` commands.
  pub description:        String,
  /// The parameters for the command, max 25.
  #[serde(skip_serializing_if = "Vec::is_empty")]
  pub options:            Vec<AppCommandOption>,
  /// Whether the command is enabled by default when the app is added to a
  /// guild. Default is true.
  pub default_permission: bool,
  /// Autoincrementing version identifier updated during substantial record
  /// changes.
  #[serde(skip_serializing_if = "Option::is_none")]
  pub version:            Option<Snowflake>,
}

#[discord_macros::number_enum]
pub enum AppCommandType {
  /// Slash commands. These can have arguments, and show up when a user types /
  ChatInput = 1,
  /// A UI-based command that shows up when you right click or tap on a user.
  User      = 2,
  /// A UI-based command that shows up when you right click or tap on a message
  Message   = 3,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AppCommandOption {
  /// The type of option.
  #[serde(rename = "type")]
  pub ty:            AppCommandOptionType,
  /// 1-32 character name.
  pub name:          String,
  /// 1-100 character description.
  pub description:   String,
  /// If the parameter is required or optional. Default is `false`.
  pub required:      bool,
  /// Choices for `String`, `Int`, and `Double` types for the user to pick from,
  /// max 25. Default is `false`.
  pub choices:       Option<Vec<AppCommandChoice>>,
  /// If the option is a subcommand or subcommand group type, this nested
  /// options will be the parameters.
  pub options:       Option<Vec<AppCommandOption>>,
  /// If the option is a channel type, the channels shown will be restricted to
  /// these types.
  pub channel_types: Option<Vec<ChannelType>>,
}

#[discord_macros::number_enum]
pub enum AppCommandOptionType {
  /// A single sub command.
  SubCommand      = 1,
  /// A group of sub commands.
  SubCommandGroup = 2,
  /// Any string. TODO: Find length limits
  String          = 3,
  /// Any integer between -2^53 and 2^53.
  Int             = 4,
  /// `true` or `false`.
  Bool            = 5,
  /// A user.
  User            = 6,
  /// Includes all channel types + categories.
  Channel         = 7,
  /// Any role name.
  Role            = 8,
  /// Includes users and roles.
  Mentionable     = 9,
  /// Any double between -2^53 and 2^53. (Discord calls this a Number, but that
  /// is unclear, so I've renamed this to `Double`).
  Double          = 10,
}

#[derive(Debug, Clone)]
pub enum AppCommandOptionValue {
  String(String),
  Int(i64),
  Bool(bool),
  User(Snowflake),
  Channel(Snowflake),
  Role(String),
  Mentionable(Snowflake),
  Double(f64),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AppCommandChoice {
  /// 1-100 character choice name.
  pub name:  String,
  /// A string, integer, or double. This is the value of the choice. If it is a
  /// string, it has a max of 100 characters.
  pub value: serde_json::Value,
}
