use crate::{util::OrderedMapKey, Snowflake};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
  /// The user's unique id.
  pub id:            Snowflake,
  /// The user's name.
  pub username:      String,
  /// The 4 digit tag next to their name.
  pub discriminator: String,
  /// The hash of their avatar.
  pub avatar:        Option<String>,
  /// True if this is a bot account.
  #[serde(default = "bool::default")]
  pub bot:           bool,
  /// True if this is an Official Discord System user.
  #[serde(default = "bool::default")]
  pub system:        bool,
  /// True if they have two factor authentication enabled.
  #[serde(default = "bool::default")]
  pub mfa_enabled:   bool,
  /// The hash of their banner.
  pub banner:        Option<String>,
  /// The rgb color code of their user color.
  pub accent_color:  Option<u32>,
  /// The user's chose language.
  pub locale:        Option<String>,
  /// Whether this user's email has been verified.
  pub verified:      Option<bool>,
  /// The user's email address.
  pub email:         Option<String>,
  /// The flags on a user's account.
  pub flags:         Option<UserFlags>,
  /// The type of nitro they have.
  pub premium_type:  Option<u32>,
  /// Any public flags on the user's account.
  pub public_flags:  Option<UserFlags>,
}

impl OrderedMapKey<Snowflake> for User {
  fn key(&self) -> Snowflake {
    self.id
  }
}

discord_macros::bitfield! {
  UserFlags,
  discord_employee             = 0,
  parterned_server_owner       = 1,
  hype_squad_events            = 2,
  bug_hunter_level_1           = 3,
  house_bravery                = 6,
  house_brilliance             = 7,
  house_balance                = 8,
  early_supporter              = 9,
  team_user                    = 10,
  bug_hunter_level_2           = 14,
  verified_bot                 = 16,
  early_verified_bot_developer = 17,
  discord_certified_moderator  = 18,
}
