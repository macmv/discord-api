use super::User;
use crate::{rest::RestSend, util::OrderedMapKey, Discord, Snowflake};
use serde::{Deserialize, Serialize};
use std::sync::{Arc, Weak};

/// A Discord channel. There are multiple types of channels: text channels and
/// voice channels, both of which are part of a guild. The other channel types
/// refer to DM (direct message) channels, for when you have direct
/// communication with a bot.
///
/// All text-based channels act similarly. You can listen for an event when a
/// message is sent, you can send messages yourself, and you can look back in
/// the chat history.
///
/// Voice channels are very different. The bot can join one voice channel per
/// guild that it has joined. Once in that voice channel, it will establish a
/// peer-to-peer connection to all other users in that voice channel, and you
/// can talk by sending Opus audio packets back and forth. At the time of
/// writing, this is unimplemented.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Channel {
  #[serde(skip)]
  pub(crate) discord:                Weak<Discord>,
  /// The ID of this channel.
  pub id:                            Snowflake,
  /// The type of channel.
  #[serde(rename = "type")]
  pub ty:                            ChannelType,
  /// The ID of the guild (may be missing for some channel objects received over
  /// gateway guild dispatches).
  pub guild_id:                      Option<Snowflake>,
  /// Sorting position of the channel.
  pub position:                      Option<u32>,
  /// Explicit permission overwrites for members and roles.
  pub permission_overwrites:         Option<Vec<PermissionOverwrite>>,
  /// The name of the channel (1-100 characters).
  pub name:                          Option<String>,
  /// The channel topic (0-1024 characters).
  pub topic:                         Option<String>,
  /// Whether the channel is NSFW (Not Safe For Work).
  pub nsfw:                          Option<bool>,
  /// The ID of the last message sent in this channel (may not point to an
  /// existing or valid message).
  pub last_message_id:               Option<Snowflake>,
  /// the bitrate (in bits) of the voice channel
  pub bitrate:                       Option<u32>,
  /// the user limit of the voice channel
  pub user_limit:                    Option<u32>,
  /// amount of seconds a user has to wait before sending another message
  /// (0-21600); bots, as well as users with the permission manage_messages or
  /// manage_channel, are unaffected
  pub rate_limit_per_user:           Option<u32>,
  /// the recipients of the DM
  pub recipients:                    Option<Vec<User>>,
  /// icon hash
  pub icon:                          Option<String>,
  /// id of the creator of the group DM or thread
  pub owner_id:                      Option<Snowflake>,
  /// application id of the group DM creator if it is bot-created
  pub application_id:                Option<Snowflake>,
  /// for guild channels: id of the parent category for a channel (each parent
  /// category can contain up to 50 channels), for threads: id of the text
  /// channel this thread was created
  pub parent_id:                     Option<Snowflake>,
  /// when the last pinned message was pinned. This may be null in events such
  /// as GUILD_CREATE when a message is not pinned.
  pub last_pin_timestamp:            Option<String>,
  /// voice region id for the voice channel, automatic when set to null
  pub rtc_region:                    Option<String>,
  /// the camera video quality mode of the voice channel, 1 when not present
  pub video_quality_mode:            Option<u32>,
  /// an approximate count of messages in a thread, stops counting at 50
  pub message_count:                 Option<u32>,
  /// an approximate count of users in a thread, stops counting at 50
  pub member_count:                  Option<u32>,
  /// thread-specific fields not needed by other channels
  pub thread_metadata:               Option<ThreadMetadata>,
  /// thread member object for the current user, if they have joined the thread,
  /// only included on certain API endpoints
  pub member:                        Option<ThreadMember>,
  /// default duration for newly created threads, in minutes, to automatically
  /// archive the thread after recent activity, can be set to: 60, 1440, 4320,
  /// 10080
  pub default_auto_archive_duration: Option<u32>,
  /// computed permissions for the invoking user in the channel, including
  /// overwrites, only included when part of the resolved data received on a
  /// slash command interaction
  pub permissions:                   Option<String>,
}

impl OrderedMapKey<Snowflake> for Channel {
  fn key(&self) -> Snowflake {
    self.id
  }
}

#[discord_macros::number_enum]
pub enum ChannelType {
  /// A text channel within a server.
  GuildText          = 0,
  /// A direct message between users.
  DM                 = 1,
  /// A voice channel within a server.
  GuildVoice         = 2,
  /// A direct message between multiple users.
  GroupDM            = 3,
  /// An category in a guild that contains up to 50 channels.
  GuildCategory      = 4,
  /// A channel that users can follow and crosspost into their own server.
  GuildNews          = 5,
  /// A channel in which game developers can sell their game on Discord.
  GuildStore         = 6,
  /// A temporary sub-channel within a `GuildNews` channel.
  GuildNewsThread    = 10,
  /// A temporary sub-channel within a `GuildText` channel.
  GuildPublicThread  = 11,
  /// A temporary sub-channel within a `GuildText` channel that is only viewable
  /// by those invited and those with the `ManageThreads` permission.
  GuildPrivateThread = 12,
  /// A voice channel for hosting events with an audience.
  GuildStageVoice    = 13,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ThreadMember {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PermissionOverwrite {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ThreadMetadata {}

/// A message recieved from the discord API. This will always have a valid id.
/// If you need to send a message, see [`MessageSend`].
#[derive(Debug, Clone, Deserialize)]
pub struct Message {
  #[serde(skip)]
  pub(crate) discord:    Weak<Discord>,
  /// The id of the message.
  pub id:                Snowflake,
  /// The channel id this message was sent in.
  pub channel_id:        Snowflake,
  /// The author of the message. Only present if this was a received message.
  pub author:            User,
  /// The message contents (up to 2000 characters). One of `content`, `file`,
  /// `embeds`, or `sticker_ids` must be present.
  pub content:           Option<String>,
  /// True if this is a TTS (Text To Speech) message.
  #[serde(default = "bool::default")]
  pub tts:               bool,
  /// The contents of the file being sent. One of content, file, embed(s),
  /// or sticker_ids must be present.
  pub file:              Option<Vec<u8>>,
  /// Embedded rich content (up to 6000 characters).
  /// One of `content`, `file`, `embeds`, or `sticker_ids` must be present.
  #[serde(default = "Vec::default")]
  pub embeds:            Vec<Embed>,
  /// JSON encoded body of non-file params, multipart/form-data only.
  pub payload_json:      Option<String>,
  /// Allowed mentions for the message. Can be used to avoid pinging everyone if
  /// @everyone is in the message. Unclear how discord handles this. I have left
  /// it out of all message sends.
  pub allowed_mentions:  Option<AllowedMentions>,
  /// Include to make your message a reply.
  pub message_reference: Option<MessageRef>,
  /// The components to include with the message.
  #[serde(default = "Vec::default")]
  pub components:        Vec<Component>,
  /// IDs of up to 3 stickers in the server to send in the message. One of
  /// content, file, embed(s), or sticker_ids must be present.
  #[serde(default = "Vec::default")]
  pub sticker_ids:       Vec<Snowflake>,
}

/// A message that will be sent to the discord API. This does not have an id,
/// and will always have `content` set. If you need to receive a message from
/// the discord API, see [`Message`].
#[derive(Debug, Clone, Default, Serialize)]
pub struct MessageSend {
  /// The message contents (up to 2000 characters). One of `content`, `file`,
  /// `embeds`, or `sticker_ids` must be present.
  pub content:           String,
  /// True if this is a TTS (Text To Speech) message.
  #[serde(skip_serializing_if = "std::ops::Not::not")]
  pub tts:               bool,
  /// The contents of the file being sent. One of content, file, embed(s),
  /// or sticker_ids must be present.
  #[serde(skip_serializing_if = "Option::is_none")]
  pub file:              Option<Vec<u8>>,
  /// Embedded rich content (up to 6000 characters).
  /// One of `content`, `file`, `embeds`, or `sticker_ids` must be present.
  #[serde(skip_serializing_if = "Vec::is_empty")]
  pub embeds:            Vec<Embed>,
  /// JSON encoded body of non-file params, multipart/form-data only.
  pub payload_json:      Option<String>,
  /// Allowed mentions for the message. Can be used to avoid pinging everyone if
  /// @everyone is in the message. Unclear how discord handles this. I have left
  /// it out of all message sends.
  #[serde(skip_serializing_if = "Option::is_none")]
  pub allowed_mentions:  Option<AllowedMentions>,
  /// Include to make your message a reply.
  #[serde(skip_serializing_if = "Option::is_none")]
  pub message_reference: Option<MessageRef>,
  /// The components to include with the message.
  #[serde(skip_serializing_if = "Vec::is_empty", default = "Vec::new")]
  pub components:        Vec<Component>,
  /// IDs of up to 3 stickers in the server to send in the message. One of
  /// content, file, embed(s), or sticker_ids must be present.
  #[serde(skip_serializing_if = "Vec::is_empty", default = "Vec::new")]
  pub sticker_ids:       Vec<Snowflake>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Component {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Embed {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AllowedMentions {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MessageRef {
  /// The id of the message.
  pub message_id: Snowflake,
  /// The id of the channel this message was sent in. Will be None for replies.
  pub channel_id: Option<Snowflake>,
  /// The id of the guild this message was sent in.
  pub guild_id:   Option<Snowflake>,
}

impl From<&str> for MessageSend {
  fn from(s: &str) -> MessageSend {
    MessageSend { content: s.into(), ..Default::default() }
  }
}

impl Message {
  #[track_caller]
  pub fn reply<M: Into<MessageSend>>(&self, msg: M) {
    let mut msg = msg.into();
    msg.message_reference =
      Some(MessageRef { message_id: self.id, channel_id: None, guild_id: None });
    self.discord().send(RestSend::MessageCreate(self.channel_id, msg));
  }

  #[track_caller]
  pub fn discord(&self) -> Arc<Discord> {
    self.discord.upgrade().expect("Trying to access Discord from Channel after Discord got dropped")
  }

  pub(crate) fn populate(&mut self, discord: &Arc<Discord>) {
    self.discord = Arc::downgrade(discord);
  }
}

impl Channel {
  #[track_caller]
  pub fn send<M: Into<MessageSend>>(&self, msg: M) {
    let msg = msg.into();
    self.discord().send(RestSend::MessageCreate(self.id, msg));
  }

  #[track_caller]
  pub fn discord(&self) -> Arc<Discord> {
    self.discord.upgrade().expect("Trying to access Discord from Channel after Discord got dropped")
  }
}
