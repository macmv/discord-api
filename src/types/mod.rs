//! This module handles all Discord API types. Each of these types can be
//! serialized/deserialized to/from json, which is how we communicate with the
//! discord servers.

mod channel;
mod command;
mod guild;
mod interaction;
mod member;
mod user;

pub use channel::*;
pub use command::*;
pub use guild::*;
pub use interaction::*;
pub use member::*;
pub use user::*;

use crate::{types::User, Snowflake};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// The data sent to us when we get the `READY` event.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ReadyInfo {
  pub application:             ReadyApp,
  pub geo_ordered_rtc_regions: Vec<String>,
  pub guild_join_requests:     Vec<String>,
  pub guilds:                  Vec<UnavailableGuild>,
  pub presences:               Vec<String>,
  pub private_channels:        Vec<String>,
  pub session_id:              String,
  pub shard:                   Vec<u32>,
  pub user:                    User,
  pub user_settings:           HashMap<String, String>,
  pub v:                       u32,
}

/// A partial Application struct, used in the `READY` gateway event.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ReadyApp {
  pub flags: u32,
  pub id:    Snowflake,
}

/// A guild where we haven't received any data about it yet. This is used when
/// we get the `READY` gateway event.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnavailableGuild {
  /// The guild ID.
  pub id:          Snowflake,
  /// Should always be true.
  pub unavailable: bool,
}
