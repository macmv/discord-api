use super::{Channel, Member, User};
use crate::{
  util::{OrderedMap, OrderedMapKey},
  Discord, Snowflake,
};
use serde::{Deserialize, Serialize};
use std::{
  collections::HashSet,
  sync::{Arc, Weak},
};

/// A Discord guild, or (more commonly) a discord server. This is the largest
/// struct in the API, and contains everything about a guild. This includes the
/// id, all the users present on the guild, the roles avaible, the channels and
/// their order, and more.
///
/// Any public field is 1:1 with the JSON api, and should be handled with some
/// care.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Guild {
  #[serde(skip)]
  pub(crate) discord:                Weak<Discord>,
  /// Guild ID.
  pub id:                            Snowflake,
  /// Guild name (2-100 characters, excluding trailing and leading whitespace).
  pub name:                          String,
  /// Icon hash.
  pub icon:                          String,
  /// Icon hash, returned when in the template object.
  pub icon_hash:                     Option<String>,
  /// Splash hash
  pub splash:                        Option<String>,
  /// Discovery splash hash; only present for guilds with the "DISCOVERABLE"
  /// feature.
  pub discovery_splash:              Option<String>,
  /// True if the user is the owner of the guild.
  pub owner:                         Option<bool>,
  /// ID of the owner.
  pub owner_id:                      Snowflake,
  /// Total permissions for the user in the guild (excludes overwrites).
  pub permissions:                   Option<String>,
  /// Voice region ID for the guild (deprecated).
  pub region:                        String,
  /// ID of afk channel.
  pub afk_channel_id:                Option<Snowflake>,
  /// AFK timeout in seconds.
  pub afk_timeout:                   u32,
  /// True if the server widget is enabled.
  pub widget_enabled:                Option<bool>,
  /// The channel id that the widget will generate an invite to, or `None` if
  /// set to no invite.
  pub widget_channel_id:             Option<Snowflake>,
  /// Verification level required for the guild.
  pub verification_level:            VerificationLevel,
  /// Default message notifications level.
  pub default_message_notifications: NotificationLevel,
  /// Explicit content filter level.
  pub explicit_content_filter:       ExplicitContentFilterLevel,
  /// Roles in the guild.
  #[serde(default = "OrderedMap::default")]
  pub roles:                         OrderedMap<Snowflake, Role>,
  /// Custom guild emojis.
  pub emojis:                        OrderedMap<Snowflake, Emoji>,
  /// Enabled guild features.
  pub features:                      HashSet<GuildFeature>,
  /// Required MFA level for the guild.
  pub mfa_level:                     MFALevel,
  /// Application id of the guild creator if it is bot-created.
  pub application_id:                Option<Snowflake>,
  /// The id of the channel where guild notices such as welcome messages and
  /// boost events are posted.
  pub system_channel_id:             Snowflake,
  /// System channel flags.
  pub system_channel_flags:          SystemChannelFlags,
  /// The id of the channel where Community guilds can display rules
  /// and/or guidelines.
  pub rules_channel_id:              Option<Snowflake>,
  /// When this guild was joined at.
  pub joined_at:                     Option<String>,
  /// True if this is considered a large guild.
  pub large:                         bool,
  /// True if this guild is unavailable due to an outage
  pub unavailable:                   bool,
  /// Number of members in this guild.
  pub member_count:                  u32,
  /// States of members currently in voice channels.
  pub voice_states:                  Vec<VoiceState>,
  /// The members in the guild.
  pub members:                       OrderedMap<Snowflake, Member>,
  /// The channels in the guild.
  pub channels:                      OrderedMap<Snowflake, Channel>,
  /// All active threads that the current user has permission to view.
  pub threads:                       OrderedMap<Snowflake, Channel>,
  /// Presences of members in the guild.
  pub presences:                     OrderedMap<Snowflake, PresenceUpdate>,
  /// Total number of members in this guild.
  pub max_presences:                 Option<u32>,
  /// The maximum number of members for the guild.
  pub max_members:                   u32,
  /// The vanity url code for the guild.
  pub vanity_url_code:               Option<String>,
  /// The description of a Community guild.
  pub description:                   Option<String>,
  /// Banner hash.
  pub banner:                        Option<String>,
  /// Premium tier (Server Boost level).
  pub premium_tier:                  PremiumTier,
  /// The number of boosts this guild currently has.
  pub premium_subscription_count:    u32,
  /// The preferred locale of a Community guild; used in server discovery
  /// and notices from Discord; defaults to "en-US".
  pub preferred_locale:              String,
  /// The id of the channel where admins and moderators of Community
  /// guilds receive notices from Discord.
  pub public_updates_channel_id:     Option<Snowflake>,
  /// The maximum amount of users in a video channel.
  pub max_video_channel_users:       u32,
  /// Approximate number of members in this guild, returned from the GET
  /// /guilds/<id> endpoint when with_counts is true.
  pub approximate_member_count:      Option<u32>,
  /// Approximate number of non-offline members in this guild, returned
  /// from the GET /guilds/<id> endpoint when with_counts is true.
  pub approximate_presence_count:    Option<u32>,
  /// The welcome screen of a Community guild, shown to new
  /// members, returned in an Invite's guild object.
  pub welcome_screen:                Option<WelcomeScreen>,
  /// Guild NSFW level.
  pub nsfw_level:                    GuildNSFWLevel,
  /// Stage instances in the guild.
  pub stage_instances:               OrderedMap<Snowflake, StageInstance>,
  /// custom guild stickers.
  pub stickers:                      OrderedMap<Snowflake, Sticker>,
}

/// A Discord role. In Discord, roles are how you give users permissions. There
/// can be up to 256 roles per guild, and every member of the guild can possess
/// any combination of roles.
///
/// Roles can only grant permissions, not remove them, so the total permissions
/// for a member can be found by combinding the permissions of all of their
/// roles. The highest role they have determines which member has seniority. See
/// the example.
///
/// Example:
///
/// - Green (makes your name green, doesn't grant permissions).
/// - Mod (gives you the ability to kick/ban others).
/// - @everyone (default role, everyone has this, and it will always be at the
///   end of the list).
///
/// If users A and B both have the Mod role, they cannot ban each other. They
/// can ban anyone without roles, but they cannot affect each other.
///
/// If user A has Mod, and user B has Mod and Green, then user B will be able to
/// ban user A. This doesn't make much sense, simply because the Green role
/// doesn't grant any permissions. However, the permission bitfield is complex,
/// and making seniority be determined based on what permissions that role has
/// would make this far too complex.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Role {
  pub id: Snowflake,
}

impl OrderedMapKey<Snowflake> for Role {
  fn key(&self) -> Snowflake {
    self.id
  }
}

/// A Discord voice state. This is the state of a user in a voice channel. It
/// contains information for if the user is muted, deafened, server muted, etc.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VoiceState {}

#[discord_macros::string_enum]
pub enum GuildFeature {
  /// Guild has access to set an animated guild icon.
  ANIMATED_ICON,
  /// Guild has access to set a guild banner image.
  BANNER,
  /// Guild has access to use commerce features (i.e. create store channels).
  COMMERCE,
  /// Guild can enable welcome screen, Membership Screening, stage channels and
  /// discovery, and receives community updates.
  COMMUNITY,
  /// Guild is able to be discovered in the directory.
  DISCOVERABLE,
  /// Guild is able to be featured in the directory.
  FEATURABLE,
  /// Guild has access to set an invite splash background.
  INVITE_SPLASH,
  /// Guild has enabled Membership Screening.
  MEMBER_VERIFICATION_GATE_ENABLED,
  /// Guild has enabled monetization.
  MONETIZATION_ENABLED,
  /// Guild has increased custom sticker slots.
  MORE_STICKERS,
  /// Guild has access to create news channels.
  NEWS,
  /// Undocumented.
  NEW_THREAD_PERMISSIONS,
  /// Guild is partnered.
  PARTNERED,
  /// Guild can be previewed before joining via Membership Screening or the
  /// directory.
  PREVIEW_ENABLED,
  /// Guild has access to create private threads.
  PRIVATE_THREADS,
  /// Guild is able to set role icons.
  ROLE_ICONS,
  /// Guild has access to the seven day archive time for threads.
  SEVEN_DAY_THREAD_ARCHIVE,
  /// Undocumented.
  THREADS_ENABLED,
  /// Guild has access to the three day archive time for threads.
  THREE_DAY_THREAD_ARCHIVE,
  /// Guild has enabled ticketed events.
  TICKETED_EVENTS_ENABLED,
  /// Guild has access to set a vanity URL.
  VANITY_URL,
  /// Guild is verified.
  VERIFIED,
  /// Guild has access to set 384kbps bitrate in voice (previously VIP voice
  /// servers).
  VIP_REGIONS,
  /// Guild has enabled the welcome screen.
  WELCOME_SCREEN_ENABLED,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PresenceUpdate {
  /// The user presence is being updated for.
  pub user:          User,
  /// ID of the guild.
  pub guild_id:      Snowflake,
  /// The user's status (online, idle, etc)
  pub status:        Status,
  /// user's current activities
  pub activities:    Vec<Activity>,
  /// user's platform-dependent status
  pub client_status: ClientStatus,
}

impl OrderedMapKey<Snowflake> for PresenceUpdate {
  fn key(&self) -> Snowflake {
    self.user.key()
  }
}

#[discord_macros::string_enum]
pub enum Status {
  /// The user is idle.
  idle,
  /// The user is set to do not disturb.
  dnd,
  /// The user is online.
  online,
  /// The user is offline or invisible.
  offline,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Activity {}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ClientStatus {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WelcomeScreen {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StageInstance {
  pub id: Snowflake,
}

impl OrderedMapKey<Snowflake> for StageInstance {
  fn key(&self) -> Snowflake {
    self.id
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Sticker {
  pub id: Snowflake,
}

impl OrderedMapKey<Snowflake> for Sticker {
  fn key(&self) -> Snowflake {
    self.id
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Emoji {
  pub id: Snowflake,
}

impl OrderedMapKey<Snowflake> for Emoji {
  fn key(&self) -> Snowflake {
    self.id
  }
}

#[discord_macros::number_enum]
pub enum NotificationLevel {
  AllMessages  = 0,
  OnlyMentions = 1,
}

// enum_type! { NotificationLevel,
//   AllMessages  = 0,
//   OnlyMentions = 1,
// }

#[discord_macros::number_enum]
pub enum ExplicitContentFilterLevel {
  Disabled            = 0,
  MembersWithoutRoles = 1,
  AllMembers          = 2,
}

#[discord_macros::number_enum]
pub enum MFALevel {
  None     = 0,
  Elevated = 1,
}

#[discord_macros::number_enum]
pub enum VerificationLevel {
  None     = 0,
  Low      = 1,
  Medium   = 2,
  High     = 3,
  VeryHigh = 4,
}

#[discord_macros::number_enum]
pub enum GuildNSFWLevel {
  Default       = 0,
  Explicit      = 1,
  Safe          = 2,
  AgeRestricted = 3,
}

#[discord_macros::number_enum]
pub enum PremiumTier {
  None  = 0,
  Tier1 = 1,
  Tier2 = 2,
  Tier3 = 3,
}

discord_macros::bitfield! {
  SystemChannelFlags,
  suppress_join_notifications           = 0,
  suppress_premium_subscriptions        = 1,
  suppress_guild_reminder_notifications = 2,
}

impl Guild {
  pub(crate) fn populate(&mut self, discord: &Arc<Discord>) {
    self.discord = Arc::downgrade(discord);
    for (i, c) in self.channels.iter_mut().enumerate() {
      c.discord = Arc::downgrade(discord);
    }
  }

  #[track_caller]
  pub fn discord(&self) -> Arc<Discord> {
    self.discord.upgrade().expect("Trying to access Discord from Guild after Discord got dropped")
  }

  /// Gets a channel by the given id. This will not make any lookup to the
  /// Discord API, since we know all the channels from all past events.
  pub fn channel(&self, id: Snowflake) -> Option<&Channel> {
    self.channels.get(&id)
  }
}
