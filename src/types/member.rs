use super::User;
use crate::{util::OrderedMapKey, Discord, Snowflake};
use serde::{Deserialize, Serialize};
use std::sync::Weak;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Member {
  #[serde(skip)]
  pub(crate) discord: Weak<Discord>,
  /// The user for this object. You should use the [`user`](Member::user)
  /// function instead, as that will look up the user if needed. This will be
  /// `None` if this `Member` is from a message create or a message update
  /// event.
  pub user:           Option<User>,
  /// The member's nickname, if they have one.
  pub nick:           Option<String>,
  /// The member's guild avatar, if they have one. This should default to the
  /// user's avatar if it is `None`.
  pub avatar:         Option<String>,
  /// The roles for this member.
  #[serde(default = "Vec::default")]
  pub roles:          Vec<Snowflake>,
  /// When the user joined.
  #[serde(default = "String::default")]
  pub joined_at:      String,
  /// When the user started boosting this guild.
  pub premium_since:  Option<u64>,
  /// Whether the user is deafened in voice channels.
  #[serde(default = "bool::default")]
  pub deaf:           bool,
  /// Whether the user is muted in voice channels.
  #[serde(default = "bool::default")]
  pub mute:           bool,
  /// Whether the user has not yet passed the membershit screening requirements.
  pub pending:        Option<bool>,
  /// The permissions of this member.
  pub permissions:    Option<String>,
}

impl OrderedMapKey<Snowflake> for Member {
  fn key(&self) -> Snowflake {
    self.user.as_ref().unwrap().key()
  }
}

/// The payload for the `GUILD_MEMBER_UPDATE` event.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MemberUpdate {
  /// The ID of the guild.
  pub guild_id:      Snowflake,
  /// User role ids.
  pub roles:         Vec<Snowflake>,
  /// The user.
  pub user:          User,
  /// Nickname of the user in the guild
  pub nick:          Option<String>,
  /// The member's guild avatar hash
  pub avatar:        Option<String>,
  /// When the user joined the guild
  pub joined_at:     Option<String>,
  /// When the user starting boosting the guild
  pub premium_since: Option<String>,
  /// Whether the user is deafened in voice channels
  pub deaf:          Option<bool>,
  /// Whether the user is muted in voice channels
  pub mute:          Option<bool>,
  /// Whether the user has not yet passed the guild's Membership Screening
  /// requirements
  pub pending:       Option<bool>,
}

impl Member {
  pub fn user(&self) -> &User {
    if let Some(u) = &self.user {
      u
    } else {
      todo!()
    }
  }
}
