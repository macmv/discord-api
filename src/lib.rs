use command::Command;
use crossbeam_channel::Sender;
use parking_lot::{Mutex, MutexGuard, RwLock, RwLockReadGuard};
use rest::{RestResp, RestSend, RestSender};
use std::{collections::HashMap, sync::Arc};
use types::{AppCommand, Guild, ReadyInfo};
use util::OrderedMap;

pub mod command;
mod gateway;
mod reader;
pub mod rest;
mod snowflake;
pub mod types;
pub mod util;

pub use gateway::{GatewayEvent, GatewayReceiver, GatewaySender, Intents, Op};
pub use reader::{DiscordReader, Event};
pub use snowflake::Snowflake;

#[derive(Debug)]
pub struct Discord {
  guilds:   Mutex<OrderedMap<Snowflake, Guild>>,
  commands: Mutex<HashMap<Snowflake, Command>>,
  ready:    RwLock<Option<ReadyInfo>>,
  rest_tx:  RestSender,
}
pub struct DiscordLock<'a> {
  guilds: MutexGuard<'a, OrderedMap<Snowflake, Guild>>,
}

pub struct DiscordWriter {
  gateway_tx: GatewaySender,
  rest_tx:    Sender<RestSend>,
  discord:    Arc<Discord>,
}

pub static DEFAULT_INTENTS: Intents = Intents(0).with_guilds().with_guild_messages();

pub fn new_bot(token: &str) -> (Arc<Discord>, DiscordReader) {
  new_raw(&format!("Bot {}", token), DEFAULT_INTENTS)
}
pub fn new_bot_intents(token: &str, intents: Intents) -> (Arc<Discord>, DiscordReader) {
  new_raw(&format!("Bot {}", token), intents)
}
pub fn new_raw(token: &str, intents: Intents) -> (Arc<Discord>, DiscordReader) {
  let gateway: HashMap<String, String> =
    ureq::get("https://discord.com/api/gateway").call().unwrap().into_json().unwrap();

  let (gateway_tx, rx) = gateway::connect(&gateway["url"], token.into(), intents);
  let rest_tx = rest::connect(token.into());
  let discord = Arc::new(Discord::new(rest_tx));

  (discord.clone(), DiscordReader::new(rx, discord))
}

impl Discord {
  pub fn new(rest_tx: RestSender) -> Self {
    Discord {
      guilds: Mutex::new(OrderedMap::new()),
      commands: Mutex::new(HashMap::new()),
      ready: RwLock::new(None),
      rest_tx,
    }
  }
  pub fn lock(&self) -> DiscordLock {
    DiscordLock { guilds: self.guilds.lock() }
  }
  pub fn send(&self, ev: RestSend) {
    self.rest_tx.send((ev, None)).unwrap();
  }
  pub fn send_resp<F: FnOnce(RestResp) + Send + 'static>(&self, ev: RestSend, on_resp: F) {
    self.rest_tx.send((ev, Some(Box::new(on_resp)))).unwrap();
  }
  pub fn ready(&self) -> RwLockReadGuard<Option<ReadyInfo>> {
    self.ready.read()
  }
  pub fn app_id(&self) -> Snowflake {
    self.ready().as_ref().expect("have not recieved the READY gateway event yet").application.id
  }
  /// Clears commands. If given a guild, this will clear commands for that
  /// guild. If given no commands, this will clear global commands for this
  /// application.
  pub fn clear_commands(self: &Arc<Self>, guild_id: Option<Snowflake>) {
    self.send(RestSend::CommandClear { app_id: self.app_id(), guild_id });
  }
  /// Adds a command. If the command has a guild set, this will only show up on
  /// one guild. If there is no guild set, then this will show up globally.
  /// However, it is only garunteed to show up within an hour if it is global.
  /// For development, it is recommended to specify a guild for this command.
  pub fn add_command(self: &Arc<Self>, mut command: Command) {
    command.inner.application_id = self.app_id();
    let s = Arc::clone(self);
    self.send_resp(RestSend::CommandCreate(command.inner.clone()), move |res| {
      let cmd: AppCommand = res.parse();
      s.commands.lock().insert(cmd.id.expect("discord did not give us a command id!"), command);
    });
  }
}

impl DiscordLock<'_> {
  pub fn guilds(&self) -> &OrderedMap<Snowflake, Guild> {
    &self.guilds
  }
  pub fn guild(&self, id: Snowflake) -> Option<&Guild> {
    self.guilds.get(&id)
  }
}
