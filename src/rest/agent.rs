use super::api;
use std::{
  collections::HashMap,
  fmt, thread,
  time::{Duration, Instant},
};
use ureq::{Agent, AgentBuilder};

pub struct RestAgent {
  agent:      Agent,
  token:      String,
  ratelimits: HashMap<String, Instant>,
}

impl RestAgent {
  pub fn new(token: String) -> Self {
    RestAgent {
      agent: AgentBuilder::new()
        .user_agent(concat!(
          "discord-api-rs (https://gitlab.com/macmv/discord-api ",
          env!("CARGO_PKG_VERSION"),
          ") ",
        ))
        .build(),
      token,
      ratelimits: HashMap::new(),
    }
  }
  pub fn post<U: fmt::Display, M: serde::Serialize>(
    &mut self,
    url: U,
    msg: &M,
  ) -> serde_json::Value {
    self.request("POST", url, msg)
  }
  pub fn put<U: fmt::Display, M: serde::Serialize>(
    &mut self,
    url: U,
    msg: &M,
  ) -> serde_json::Value {
    self.request("PUT", url, msg)
  }

  pub fn request<U: fmt::Display, M: serde::Serialize>(
    &mut self,
    method: &str,
    url: U,
    msg: &M,
  ) -> serde_json::Value {
    let url = url.to_string();
    if let Some(reset) = self.ratelimits.get(&url) {
      if &Instant::now() > reset {
        self.ratelimits.remove(&url);
      } else {
        let dur = reset.duration_since(Instant::now());
        println!("WAITING {:?}", &dur);
        thread::sleep(dur);
        self.ratelimits.remove(&url);
      }
    }
    let res = self
      .agent
      .request(method, &format!("{}/{}", api(), url))
      .set("Content-Type", "application/json")
      .set("Authorization", &self.token)
      .send_json(serde_json::to_value(msg).unwrap())
      .unwrap();

    // These are the ratelimit headers we have to work with:
    //
    // - X-RateLimit-Bucket
    //   - A string, which should be a unique id for each rate limit.
    // - X-RateLimit-Limit
    //   - An integer, which is the total number of requests that can be sent.
    // - X-RateLimit-Remaining
    //   - An integer, which is the number of remaining requests until we are
    //     blocked.
    // - X-RateLimit-Reset
    //   - A float, which is a unix timestamp for when the ratelimit is cleared.
    // - X-RateLimit-Reset-After
    //   - A float, which is a number of seconds until the ratelimit is cleared.
    //
    // The bucket is supposed to tell us which rate limit we are up against, but it
    // isn't actually right. For example, the bucket will be the same for all
    // message send events, but the actual number of remaining attempts for each
    // message send endpoint will be different.
    //
    // This is most likely due to discord not syncing all of their rate limiting
    // information between every request (because that would be insane). This ends
    // up meaning that the bucket ids are more of a suggestion. Because of that, I
    // have chosen to ignore them completely, and just use the X-RateLimit-Remaining
    // header.

    let remaining: i32 = res.header("X-RateLimit-Remaining").unwrap().parse().unwrap();
    let reset: f64 = res.header("X-RateLimit-Reset-After").unwrap().parse().unwrap();

    if remaining == 0 {
      self.ratelimits.insert(url, Instant::now() + Duration::from_secs_f64(reset));
    }

    res.into_json().unwrap()
  }
}
