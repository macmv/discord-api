use crate::{
  types::{AppCommand, MessageSend},
  Snowflake,
};
use crossbeam_channel::{bounded, Sender};
use serde::de::DeserializeOwned;
use serde_json::json;
use std::thread;

mod agent;

use agent::RestAgent;

pub type RestSender = Sender<RestMessage>;
pub type RestMessage = (RestSend, Option<Box<dyn FnOnce(RestResp) + Send>>);

pub enum RestSend {
  MessageCreate(Snowflake, MessageSend),
  CommandClear { app_id: Snowflake, guild_id: Option<Snowflake> },
  CommandCreate(AppCommand),
}

#[derive(Clone, Debug)]
pub struct RestResp {
  res: serde_json::Value,
}

pub fn connect(token: String) -> RestSender {
  let (tx, rx) = bounded::<RestMessage>(256);

  thread::spawn(move || {
    let mut agent = RestAgent::new(token);
    loop {
      let (ev, handle) = rx.recv().unwrap();
      let res = match ev {
        RestSend::MessageCreate(id, msg) => {
          let res = agent.post(format!("channels/{}/messages", id), &msg);
          println!("got res: {}", res);
          res
        }
        RestSend::CommandClear { app_id, guild_id } => {
          let res = agent.put(
            match guild_id {
              Some(gid) => {
                format!("applications/{}/guilds/{}/commands", app_id, gid)
              }
              None => format!("applications/{}/commands", app_id),
            },
            &json!(""),
          );
          res
        }
        RestSend::CommandCreate(command) => {
          println!("sending command {}", serde_json::to_string(&command).unwrap());
          let res = agent.post(
            match command.guild_id {
              Some(gid) => {
                format!("applications/{}/guilds/{}/commands", command.application_id, gid)
              }
              None => format!("applications/{}/commands", command.application_id),
            },
            &command,
          );
          println!("got res: {}", res);
          res
        }
      };
      if let Some(handle) = handle {
        handle(RestResp { res });
      }
    }
  });

  tx
}

pub fn api() -> String {
  format!("https://discord.com/api/v{}", ver())
}

pub const fn ver() -> &'static str {
  "9"
}

impl RestResp {
  pub fn parse<T: DeserializeOwned>(self) -> T {
    serde_json::from_value(self.res).unwrap()
  }
}
