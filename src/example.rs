use discord_api::{
  command::{CommandBuilder, CommandOption, OptionType},
  Event, Snowflake,
};
use std::{fs, io};

fn main() -> io::Result<()> {
  let token = fs::read_to_string("token.txt")?;
  let token = token.trim();

  let (state, rx) = discord_api::new_bot(token);

  // let s = state.clone();
  // thread::spawn(move || loop {
  //   thread::sleep(Duration::from_millis(1500));
  //   s.lock()
  //     .guild(Snowflake(704421462475407411))
  //     .map(|g| g.channel(Snowflake(704421462475407414)).map(|c| c.send("big
  // gaming"))); });

  for ev in rx {
    match ev {
      Event::Ready => {
        state.add_command(
          CommandBuilder::new("big")
            .for_guild(Snowflake(704421462475407411))
            .add_option(CommandOption::new("user").ty(OptionType::User))
            .add_option(CommandOption::new("amount").ty(OptionType::Int))
            .build(|_, int| println!("GOT COMMAND arg: {:?}", int.option::<Snowflake>("user"))),
        );
      }
      Event::GuildCreate(guild) => {
        if guild.id == 704421462475407411 {
          let chan = guild.channel(Snowflake(704421462475407414)).unwrap();
          chan.send("big gaming time");
        }
        // for _ in 0..20 {
        //   guild.channel(Snowflake(704421462475407414)).map(|c| c.send("big
        // gaming time")); }
      }
      Event::MessageCreate(m) => {
        if !m.author.bot {
          m.reply("big gaming time");
        }
        println!("got message: {:?}", m);
      }
      _ => {}
    }
  }

  println!("hello world!");

  Ok(())
}
