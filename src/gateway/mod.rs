use crossbeam_channel::{bounded, Receiver, SendError, Sender, TryRecvError};
use mio::{net::TcpStream, Events, Interest, Poll, Token, Waker};
use serde::de::DeserializeOwned;
use serde_json::json;
use std::{
  io,
  io::{Read, Write},
  net::ToSocketAddrs,
  sync::{
    atomic::{AtomicBool, AtomicU32, Ordering},
    Arc,
  },
  thread,
  time::Duration,
};
use tungstenite::{HandshakeError, Message, WebSocket};

mod types;
pub use types::*;

#[derive(Debug)]
pub struct GatewayReceiver {
  rx:        Receiver<GatewayEvent>,
  tx:        GatewaySender,
  got_hello: AtomicBool,
  token:     String,
  intents:   Intents,
}
#[derive(Debug, Clone)]
pub struct GatewaySender {
  waker:     Arc<Waker>,
  tx:        Sender<GatewayEvent>,
  last_sent: Arc<AtomicU32>,
}

pub fn connect(gateway: &str, token: String, intents: Intents) -> (GatewaySender, GatewayReceiver) {
  let host = gateway
    .strip_prefix("wss://")
    .unwrap_or_else(|| panic!("gateway '{gateway}' did not start with 'wss://'"));
  let req = format!("{gateway}/?v={}&encoding=json", crate::rest::ver());

  const WAKE: Token = Token(0);
  const CLIENT: Token = Token(1);

  let mut client =
    TcpStream::connect(format!("{host}:443").to_socket_addrs().unwrap().next().unwrap())
      .unwrap_or_else(|e| panic!("failed to connect to gateway: {e}"));

  let mut poll = Poll::new().unwrap();
  let mut events = Events::with_capacity(128);

  let waker = Waker::new(poll.registry(), WAKE).unwrap();
  poll.registry().register(&mut client, CLIENT, Interest::READABLE | Interest::WRITABLE).unwrap();

  let (mut gateway_client, _) = match tungstenite::client_tls(req, client) {
    Ok(v) => v,
    Err(HandshakeError::Interrupted(e)) => {
      let mut err = e;
      loop {
        match err.handshake() {
          Ok(v) => break v,
          Err(HandshakeError::Interrupted(e)) => err = e,
          Err(e) => panic!("failed to connect to gateway: {e}"),
        }
      }
    }
    Err(e) => panic!("failed to connect to gateway: {e}"),
  };

  let (send_tx, send_rx) = bounded(256);
  let (recv_tx, recv_rx) = bounded(256);

  thread::spawn(move || loop {
    fn send_all<T: Read + Write>(
      gateway_client: &mut WebSocket<T>,
      send_rx: &Receiver<GatewayEvent>,
    ) {
      loop {
        match send_rx.try_recv() {
          Ok(m) => {
            let text = serde_json::to_string(&m).unwrap();
            println!("sending: {}", &text);
            gateway_client.write_message(Message::Text(text)).unwrap();
          }
          Err(TryRecvError::Empty) => break,
          Err(TryRecvError::Disconnected) => panic!("channel sender closed"),
        }
      }
    }
    fn recv_all<T: Read + Write>(
      gateway_client: &mut WebSocket<T>,
      recv_tx: &Sender<GatewayEvent>,
    ) {
      loop {
        match gateway_client.read_message() {
          Ok(Message::Text(text)) => {
            let m: GatewayEvent = serde_json::from_str(&text).unwrap();
            recv_tx.send(m).unwrap();
          }
          Ok(v) => panic!("unknown message: {}", v),
          Err(tungstenite::error::Error::Io(e)) if e.kind() == io::ErrorKind::WouldBlock => break,
          Err(e) => panic!("failed to read from websocket: {}", e),
        }
      }
    }

    poll.poll(&mut events, None).unwrap();

    for ev in events.iter() {
      match ev.token() {
        // We just got woken up, so there is some data to send
        WAKE => send_all(&mut gateway_client, &send_rx),
        CLIENT => {
          if ev.is_readable() {
            recv_all(&mut gateway_client, &recv_tx);
          }
          if ev.is_writable() {
            send_all(&mut gateway_client, &send_rx);
          }
        }
        _ => unreachable!(),
      }
    }
  });

  let send =
    GatewaySender { waker: waker.into(), tx: send_tx, last_sent: Arc::new(0.into()) };
  let recv =
    GatewayReceiver { rx: recv_rx, tx: send.clone(), got_hello: false.into(), token, intents };

  (send, recv)
}

impl GatewayReceiver {
  pub fn recv(&mut self) -> Option<GatewayEvent> {
    loop {
      if let Ok(m) = self.rx.recv() {
        match Op::from(m.op) {
          Op::GatewayHello => {
            if self.got_hello.swap(true, Ordering::Relaxed) {
              panic!("already recieved a gateway hello!");
            }
            let interval = Duration::from_millis(
              m.d.as_ref().unwrap().as_object().unwrap()["heartbeat_interval"].as_u64().unwrap(),
            );
            let tx = self.tx.clone();
            println!("got gateway interval: {:?}", interval);
            thread::spawn(move || {
              thread::sleep(interval / 2); // TODO: Should be rand(0, interval)
              loop {
                tx.send_heartbeat().unwrap();
                thread::sleep(interval);
              }
            });

            self
              .tx
              .send(GatewayEvent {
                op: Op::Identify.into(),
                d:  Some(json!({
                  "token": self.token,
                  "properties": {
                    "$os": "linux",
                    "$browser": "disco",
                    "$device": "disco"
                  },
                  // TODO: Compression
                  "compress": false,
                  "large_threshold": 250,
                  "shard": [0, 1],
                  "presence": {
                    "activities": [{
                      "name": "I am cool",
                      "type": 0
                    }],
                    "status": "online",
                    "since": 91879201,
                    "afk": false
                  },
                  "intents": self.intents,
                })),
                s:  None,
                t:  None,
              })
              .unwrap();
          }
          Op::HeartbeatAck => {
            println!("got ack: {:?}", m);
          }
          _ => break Some(m),
        }
      } else {
        break None;
      }
    }
  }
}

impl GatewaySender {
  pub fn send_heartbeat(&self) -> Result<(), SendError<GatewayEvent>> {
    let last_sent = self.last_sent.fetch_add(1, Ordering::Acquire);
    self.send(GatewayEvent {
      op: Op::Heartbeat.into(),
      d:  Some(json!(last_sent + 1)),
      t:  None,
      s:  None,
    })
  }

  pub fn send(&self, ev: GatewayEvent) -> Result<(), SendError<GatewayEvent>> {
    self.tx.send(ev)?;
    self.waker.wake().unwrap();
    Ok(())
  }
}

impl GatewayEvent {
  pub fn parse<T: DeserializeOwned>(self) -> T {
    dbg!(self.d.as_ref().unwrap());
    serde_json::from_value(self.d.unwrap()).unwrap()
  }
}
