use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GatewayEvent {
  pub op: u32,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub d:  Option<serde_json::Value>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub s:  Option<u32>,
  #[serde(skip_serializing_if = "Option::is_none")]
  pub t:  Option<String>,
}

#[discord_macros::number_enum]
pub enum Op {
  Dispatch       = 0,
  Heartbeat      = 1,
  Identify       = 2,
  Reconnect      = 7,
  InvalidSession = 9,
  GatewayHello   = 10,
  HeartbeatAck   = 11,
}

discord_macros::bitfield! {
  Intents,
  /// This intent enables these events:
  /// - [`GUILD_CREATE`](crate::Event::GuildCreate)
  /// - `GUILD_UPDATE` (unimplemented)
  /// - `GUILD_DELETE` (unimplemented)
  /// - `GUILD_ROLE_CREATE` (unimplemented)
  /// - `GUILD_ROLE_UPDATE` (unimplemented)
  /// - `GUILD_ROLE_DELETE` (unimplemented)
  /// - `CHANNEL_CREATE` (unimplemented)
  /// - `CHANNEL_UPDATE` (unimplemented)
  /// - `CHANNEL_DELETE` (unimplemented)
  /// - `CHANNEL_PINS_UPDATE` (unimplemented)
  /// - `THREAD_CREATE` (unimplemented)
  /// - `THREAD_UPDATE` (unimplemented)
  /// - `THREAD_DELETE` (unimplemented)
  /// - `THREAD_LIST_SYNC` (unimplemented)
  /// - `THREAD_MEMBER_UPDATE` (unimplemented)
  /// - `THREAD_MEMBERS_UPDATE *` (unimplemented)
  /// - `STAGE_INSTANCE_CREATE` (unimplemented)
  /// - `STAGE_INSTANCE_UPDATE` (unimplemented)
  /// - `STAGE_INSTANCE_DELETE` (unimplemented)
  guilds = 0,

  /// This intent enables these events:
  /// - `GUILD_MEMBER_ADD` (unimplemented)
  /// - [`GUILD_MEMBER_UPDATE`](crate::Event::MemberUpdate)
  /// - `GUILD_MEMBER_REMOVE` (unimplemented)
  /// - `THREAD_MEMBERS_UPDATE *` (unimplemented)
  ///
  /// This is a special intent. You must enable this in the
  /// [portal](https://discord.com/developers/applications)
  /// in order to receive these events. Specifically, go to
  /// the 'bots' tab on the left, and enable the 'Server Members
  /// Intent' check box.
  guild_members = 1,

  /// This intent enables these events:
  /// - `GUILD_BAN_ADD` (unimplemented)
  /// - `GUILD_BAN_REMOVE` (unimplemented)
  guild_bans = 2,

  /// This intent enables these events:
  /// - `GUILD_EMOJIS_UPDATE` (unimplemented)
  /// - `GUILD_STICKERS_UPDATE` (unimplemented)
  guild_emojis_and_stickers = 3,

  /// This intent enables these events:
  /// - `GUILD_INTEGRATIONS_UPDATE` (unimplemented)
  /// - `INTEGRATION_CREATE` (unimplemented)
  /// - `INTEGRATION_UPDATE` (unimplemented)
  /// - `INTEGRATION_DELETE` (unimplemented)
  guild_integrations = 4,

  /// This intent enables these events:
  /// - `WEBHOOKS_UPDATE` (unimplemented)
  guild_webhooks = 5,

  /// This intent enables these events:
  /// - `INVITE_CREATE` (unimplemented)
  /// - `INVITE_DELETE` (unimplemented)
  guild_invites = 6,

  /// This intent enables these events:
  /// - `VOICE_STATE_UPDATE` (unimplemented)
  guild_voice_states = 7,

  /// This intent enables these events:
  /// - `PRESENCE_UPDATE` (unimplemented)
  ///
  /// This is a special intent. You must enable this in the
  /// [portal](https://discord.com/developers/applications)
  /// in order to receive these events. Specifically, go to
  /// the 'bots' tab on the left, and enable the 'Presence
  /// Intent' check box.
  guild_presences = 8,

  /// This intent enables these events (only in guilds, not direct messages):
  /// - `MESSAGE_CREATE` (unimplemented)
  /// - `MESSAGE_UPDATE` (unimplemented)
  /// - `MESSAGE_DELETE` (unimplemented)
  /// - `MESSAGE_DELETE_BULK` (unimplemented)
  guild_messages = 9,

  /// This intent enables these events (only in guilds, not direct messages):
  /// - `MESSAGE_REACTION_ADD` (unimplemented)
  /// - `MESSAGE_REACTION_REMOVE` (unimplemented)
  /// - `MESSAGE_REACTION_REMOVE_ALL` (unimplemented)
  /// - `MESSAGE_REACTION_REMOVE_EMOJI` (unimplemented)
  guild_message_reactions = 10,

  /// This intent enables these events (only in guilds, not direct messages):
  /// - `TYPING_START` (unimplemented)
  guild_message_typing = 11,

  /// This intent enables these events (only in direct messages):
  /// - `MESSAGE_CREATE` (unimplemented)
  /// - `MESSAGE_UPDATE` (unimplemented)
  /// - `MESSAGE_DELETE` (unimplemented)
  /// - `CHANNEL_PINS_UPDATE` (unimplemented)
  direct_messages = 12,

  /// This intent enables these events (only in direct messages):
  /// - `MESSAGE_REACTION_ADD` (unimplemented)
  /// - `MESSAGE_REACTION_REMOVE` (unimplemented)
  /// - `MESSAGE_REACTION_REMOVE_ALL` (unimplemented)
  /// - `MESSAGE_REACTION_REMOVE_EMOJI` (unimplemented)
  direct_message_reactions = 13,

  /// This intent enables these events (only in direct messages):
  /// - `TYPING_START` (unimplemented)
  direct_message_typing = 14,
}
