use crate::{
  types::{AppCommand, AppCommandOption, AppCommandOptionType, AppCommandType, InteractionData},
  Snowflake,
};
use std::fmt;

/// A Discord slash command. This is seperate from the
/// `types::ApplicationCommand` struct, as this handles callbacks for commands
/// automatically. The `ApplicationCommand` struct will simply setup a command
/// with discord, wheras this will add handlers and such for you.
pub struct Command {
  pub(crate) inner: AppCommand,
  cb:               Box<dyn Fn(Snowflake, &InteractionData) + Send>,
}

impl fmt::Debug for Command {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    self.inner.fmt(f)
  }
}

/// A command builder. This will help setup all the choices/options to create
/// your command. Discord heavily rate limits creating commands, so it is
/// recommended to only send you command once. Using the command builder will
/// ensure that you are only able to send it once you have finished building it.
pub struct CommandBuilder {
  inner: AppCommand,
}

pub struct CommandOption {
  inner: AppCommandOption,
}

pub enum OptionType {
  /// Any string.
  String,
  /// Any integer between -2^53 and 2^53.
  Int,
  /// `true` or `false`.
  Bool,
  /// A user.
  User,
  /// Includes all channel types + categories.
  Channel,
  /// Any role name.
  Role,
  /// Includes users and roles.
  Mentionable,
  /// Any double between -2^53 and 2^53. (Discord calls this a Number, but that
  /// is unclear, so I've renamed this to `Double`).
  Double,
}

impl CommandBuilder {
  pub fn new<N: Into<String>>(name: N) -> Self {
    CommandBuilder {
      inner: AppCommand {
        id:                 None,
        ty:                 AppCommandType::ChatInput,
        application_id:     Snowflake(0),
        guild_id:           None,
        name:               name.into(),
        description:        "big command".into(),
        options:            vec![],
        default_permission: true,
        version:            None,
      },
    }
  }

  /// By default, commands are global. However, those commands are cached for up
  /// to an hour. For testing, it is much more convienient to have a command
  /// that exists on one guild. This command will update instantly, and will
  /// still do everything that a global command does.
  pub fn for_guild(mut self, guild: Snowflake) -> Self {
    self.inner.guild_id = Some(guild);
    self
  }

  /// Adds the given option to a command. This option is a section of the
  /// command that the user will enter. Each option can have multiple
  /// [`Choice`]s associated with it, and they will choose between those
  /// choices when entering a value for the option.
  ///
  /// # Panics
  ///
  /// Panics if the command has more than 25 options. A discord command can only
  /// have a max of 25 options.
  #[track_caller]
  pub fn add_option(mut self, option: CommandOption) -> Self {
    self.inner.options.push(option.inner);
    if self.inner.options.len() > 25 {
      panic!("can only have up to 25 options on a single command");
    }
    self
  }

  /// Builds this into a finialized command. Once this is called, you cannot
  /// modify the options of this command anymore.
  pub fn build<F: Fn(Snowflake, &InteractionData) + 'static + Send>(self, cb: F) -> Command {
    Command { inner: self.inner, cb: Box::new(cb) }
  }
}

impl CommandOption {
  pub fn new<N: Into<String>>(name: N) -> Self {
    CommandOption {
      inner: AppCommandOption {
        name:          name.into(),
        ty:            AppCommandOptionType::String,
        description:   "hello".into(),
        required:      true,
        choices:       Some(vec![]),
        options:       Some(vec![]),
        channel_types: Some(vec![]),
      },
    }
  }

  /// Sets the type of this option.
  ///
  /// # Panics
  ///
  /// Panics if the inner choices list is not empty. This is to simplify user
  /// code. Any choices added must be valid for this option type, and should be
  /// added after you have specified the type of option.
  ///
  /// Changing the type of option is valid, but is bad practice.
  #[track_caller]
  pub fn ty(mut self, new_ty: OptionType) -> Self {
    if !self.inner.choices.as_ref().unwrap().is_empty() {
      panic!("cannot change option type after adding choices");
    }
    self.inner.ty = new_ty.into();
    self
  }
}

impl From<OptionType> for AppCommandOptionType {
  fn from(op: OptionType) -> Self {
    match op {
      OptionType::String => Self::String,
      OptionType::Int => Self::Int,
      OptionType::Bool => Self::Bool,
      OptionType::User => Self::User,
      OptionType::Channel => Self::Channel,
      OptionType::Role => Self::Role,
      OptionType::Mentionable => Self::Mentionable,
      OptionType::Double => Self::Double,
    }
  }
}

impl Command {
  pub(crate) fn run(&self, user_id: Snowflake, data: &InteractionData) {
    (self.cb)(user_id, data);
  }
}
